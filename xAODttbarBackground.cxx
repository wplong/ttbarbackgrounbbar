#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>
#include <ttbarBackground/xAODttbarBackground.h>

// Infrastructure include(s):
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"

// ASG status code check
#include <AsgTools/MessageCheck.h>

// Truth level includes:
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthVertex.h"

// Jet
#include "xAODJet/JetContainer.h"
//#include "xAODJet/JetAuxContainer.h"

#include <TFile.h>

#include "Riostream.h"
#include <iostream>
#include <cstdlib>
#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"
#include "JetMomentTools/JetVertexTaggerTool.h"

// EDM includes:
#include "xAODEventInfo/EventInfo.h"

#include "xAODCore/ShallowCopy.h"

// this is needed to distribute the algorithm to the workers
ClassImp(xAODttbarBackground)

bool jetpTCompare (const xAOD::Jet *a, const xAOD::Jet *b) {
  return a->pt() > b->pt();
}

xAODttbarBackground :: xAODttbarBackground ()
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  Note that you should only put
  // the most basic initialization here, since this method will be
  // called on both the submission and the worker node.  Most of your
  // initialization code will go into histInitialize() and
  // initialize().
}



EL::StatusCode xAODttbarBackground :: setupJob (EL::Job& job)
{
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.


  // let's initialize the algorithm to use the xAODRootAccess package
  job.useXAOD ();
  //  xAOD::Init(); // call before opening first file

  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)
  ANA_CHECK(xAOD::Init());

  
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODttbarBackground :: histInitialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  TFile *outputFile = wk()->getOutputFile (outputName);
  tree = new TTree ("tree", "tree");
  tree->SetDirectory(outputFile);


  // initialization b-quark level 
  /****************************************************************************************************************************************************************/

  /* -> histogram initialization */

  bPT = new TH1F("bPT", "bPT", 120, 0, 600); // 
  bPT->GetXaxis()->SetTitle("b-quark pT from ttbar");
  wk()->addOutput(bPT);

  bEta = new TH1F("bEta", "bEta", 200, -10, 10); // eta
  bEta->GetXaxis()->SetTitle("b-quark eta from ttbar");
  wk()->addOutput(bEta);

  deltaPhi = new TH1F("deltaPhi", "deltaPhi", 200, -8, 8); 
  deltaPhi->GetXaxis()->SetTitle("b-quark deltaPhi from ttbar");
  wk()->addOutput(deltaPhi);

  bPTW = new TH1F("bPTW", "bPTW", 120, 0, 600); 
  bPTW->GetXaxis()->SetTitle("b-quark Pt from W");
  wk()->addOutput(bPTW);

  bPTAll = new TH1F("bPTAll", "bPTAll", 120, 0, 600); 
  bPTAll->GetXaxis()->SetTitle("all b-quarks Pt -- ttbar");
  wk()->addOutput(bPTAll);

  nParent = new TH1F("nParent", "nParent", 5, 0, 5); 
  nParent->GetXaxis()->SetTitle("nParent of b-quark");
  wk()->addOutput(nParent);

  parentPdgId = new TH1F("parentPdgId", "parentPdgId", 50, 0, 50); // eta
  parentPdgId->GetXaxis()->SetTitle("b-quark parent pdgID");
  wk()->addOutput(parentPdgId);

  dquarknParent = new TH1F("dquarknParent", "dquarknParent", 5, 0, 5); // eta
  dquarknParent->GetXaxis()->SetTitle("b-quark n parent");
  wk()->addOutput(dquarknParent);


  bPTFromTquark = new TH1F("bPTFromTquark", "bPTFromTquark", 120, 0, 600); 
  bPTFromTquark->GetXaxis()->SetTitle("b-quarks from t-quark");
  wk()->addOutput(bPTFromTquark);

  bPTFromUquark = new TH1F("bPTFromUquark", "bPTFromUquark", 120, 0, 600); 
  bPTFromUquark->GetXaxis()->SetTitle("b-quarks from u-quark");
  wk()->addOutput(bPTFromUquark);

  bPTFromGluon = new TH1F("bPTFromGluon", "bPTFromGluon", 120, 0, 600); 
  bPTFromGluon->GetXaxis()->SetTitle("b-quarks from gluon");
  wk()->addOutput(bPTFromGluon);


  /* -> tree branch initialization */

  tree->Branch("b1PT", &b1PT);
  tree->Branch("b2PT", &b2PT);

  tree->Branch("b1Eta", &b1Eta);
  tree->Branch("b2Eta", &b2Eta);

  tree->Branch("b1Phi", &b1Phi);
  tree->Branch("b2Phi", &b2Phi);

  tree->Branch("b1M", &b1M);
  tree->Branch("b2M", &b2M);

  tree->Branch("b1Px", &b1Px);
  tree->Branch("b1Py", &b1Py);
  tree->Branch("b1Pz", &b1Pz);

  tree->Branch("b2Px", &b2Px);
  tree->Branch("b2Py", &b2Py);
  tree->Branch("b2Pz", &b2Pz);

  tree->Branch("b1b2DeltaPhi", &b1b2DeltaPhi);


  // initialization b-jet level
  /****************************************************************************************************************************************************************/
  // NEVER touch the code before this line EVER!

  //bTaggingCalibrationTool initilization
  /****************************************/
  btagtool = new BTaggingSelectionTool("BTaggingSelectionTool");
  btagtool->setProperty("MaxEta", 2.5);
  btagtool->setProperty("MinPt", 20000.);
  btagtool->setProperty("JetAuthor", "AntiKt4EMTopoJets");
  btagtool->setProperty("TaggerName", "MV2c20");
  btagtool->setProperty("FlvTagCutDefinitionsFileName", "xAODBTaggingEfficiency/13TeV/2016-13TeV-WorkingPointsOnly-Release20_7-AntiKt4EMTopoJets-May3.root"); // "xAODBTaggingEfficiency/13TeV/2015-PreRecomm-13TeV-MC12-CDI-October23_v1.root" );
  btagtool->setProperty("OperatingPoint", "FixedCutBEff_85");
  btagtool->initialize();


  // jetVertextool initialization
  /****************************************/
  pjvtag = new JetVertexTaggerTool("jvtag");
  hjvtagup = ToolHandle<IJetUpdateJvt>("jvtag");
  //  bool fail = false;
  fail |= pjvtag->setProperty("JVTFileName","JetMomentTools/JVTlikelihood_20140805.root").isFailure();
  fail |= pjvtag->initialize().isFailure();

  if ( fail ) {
    std::cout << "Tool initialialization failed!" << std::endl;
    return 1;
  }
  

  /* -> tree initialization */
  /****************************************/
  tree->Branch("nOfJetsBranch", &nOfJets);
  tree->Branch("nOfJetsTaggedBranch", &nOfJetsTagged);

  tree->Branch("bJetPt", &bJetPt);
  tree->Branch("bJetTaggedPt", &bJetTaggedPt);
  tree->Branch("bJetTaggedUpdatedPt", &bJetTaggedUpdatedPt);

  tree->Branch("bJetLeadingPt", &bJetLeadingPt);
  tree->Branch("bJetSubleadingPt", &bJetSubleadingPt);
  tree->Branch("bJetThirdleadingPt", &bJetThirdleadingPt);
  tree->Branch("bJetFourthleadingPt", &bJetFourthleadingPt);
  tree->Branch("bJetFifthleadingPt", &bJetFifthleadingPt);

  tree->Branch("bJetLeadingMass", &bJetLeadingMass);
  tree->Branch("bJetSubleadingMass", &bJetSubleadingMass);
  tree->Branch("bJetThirdleadingMass", &bJetThirdleadingMass);
  tree->Branch("bJetFourthleadingMass", &bJetFourthleadingMass);
  tree->Branch("bJetFifthleadingMass", &bJetFifthleadingMass);

  tree->Branch("j1j2Mass", &j1j2Mass);
  tree->Branch("j1j2DeltaR", &j1j2DeltaR);

  tree->Branch("j1j2MassDeltaRSmall", &j1j2MassDeltaRSmall);
  tree->Branch("j1j2MassDeltaRLarge", &j1j2MassDeltaRLarge);

  tree->Branch("j1j3Mass", &j1j3Mass);

  tree->Branch("nOfJetsOpposite", &nOfJetsOpposite);

  tree->Branch("j1j2MassCone", &j1j2MassCone);
  
  return EL::StatusCode::SUCCESS;
}//end of histinitialize



EL::StatusCode xAODttbarBackground :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODttbarBackground :: changeInput (bool firstFile)
{
  // Here you do everything you need to do when we change input files,
  // e.g. resetting branch addresses on trees.  If you are using
  // D3PDReader or a similar service this method is not needed.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODttbarBackground :: initialize ()
{
  // Here you do everything that you need to do after the first input
  // file has been connected and before the first event is processed,
  // e.g. create additional histograms based on which variables are
  // available in the input files.  You can also create all of your
  // histograms and trees in here, but be aware that this method
  // doesn't get called if no events are processed.  So any objects
  // you create here won't be available in the output if you have no
  // input events.

  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)
  xAOD::TEvent* event = wk()->xaodEvent();

  // as a check, let's see the number of events in our xAOD
  Info("initialize()", "Number of events = %lli", event->getEntries() ); // print long long int
  

  // Jet Calibration
  const std::string name = "xAODttbarBackground"; // string describing the current thread, for logging
  TString jetAlgo        = "AntiKt4EMTopo";   // String describing your jet collection, for example AntiKt4EMTopo or AntiKt4LCTopo (see above)
  TString config         = "JES_MC15cRecommendation_May2016.config"; // Path to global config used to initialize the tool (see above)
  TString calibSeq       = "JetArea_Residual_Origin_EtaJES_GSC";     // String describing the calibration sequence to apply (see above)
  bool    isData         = false;                  // bool describing if the events are data or from simulation

  m_jetCalibration = new JetCalibrationTool(name);
  ANA_CHECK(m_jetCalibration->setProperty("JetCollection", jetAlgo.Data()));
  ANA_CHECK(m_jetCalibration->setProperty("ConfigFile", config.Data()));
  ANA_CHECK(m_jetCalibration->setProperty("CalibSequence", calibSeq.Data()));
  ANA_CHECK(m_jetCalibration->setProperty("IsData", isData));

  // Initialize the tool
  ANA_CHECK(m_jetCalibration->initializeTool(name));


  return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODttbarBackground :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  // test for push script

  // set type of return code you are expecting (add to top of each function once)
  ANA_CHECK_SET_TYPE (EL::StatusCode); 
  xAOD::TEvent* event = wk()->xaodEvent();


  // analysis code for QUARK level
  /****************************************************************************************************************************************************************/
  ANA_CHECK(event->retrieve(truthP, "TruthParticles"));

  tQuark.clear();
  bQuark.clear();
  dpPdgId.clear();

  //  
  // loop over truthP in the container
  for (const auto &truthp_itr : *truthP) {         // for each truth particle in the containter
    
    // check b-quark from t-quark
    if(truthp_itr->absPdgId() != 5)
      continue;
    else if(truthp_itr->nParents() != 1)
      continue;
    else if(truthp_itr->parent(0)->absPdgId() != 6)
      continue;
    else {

      //      bPTFromTquark->Fill( truthp_itr->pt() * 0.001 );
      bQuark.push_back(truthp_itr);
      //      bPT->Fill(truthp_itr->pt() * 0.001);
    }
  }// end of for


  // check where these b-quarks are from
  for (const auto &truthp_itr : *truthP) {         // for each truth particle in the containter
    // check all b-quark
    if (truthp_itr->absPdgId() != 5)
      continue;
    else if (truthp_itr->nChildren() == 1)
      continue;
    else {
      bPTAll->Fill(truthp_itr->pt() * 0.001);
      nParent->Fill( truthp_itr->nParents() );
      parentPdgId->Fill( truthp_itr->parent(0)->absPdgId() );

      /** temporary
      if (truthp_itr->parent(0)->absPdgId() == 5) {
	// ouput the parent of b-quark that is from b-quark
	if (truthp_itr->parent(0)->parent(0)->absPdgId() == 21)
	  std::cout << truthp_itr->parent(0)->parent(0)->nChildren() << "----> number of children of g->b->b" << std::endl;

	if (truthp_itr->parent(0)->parent(0)->absPdgId() == 2)
	  std::cout << truthp_itr->parent(0)->parent(0)->nChildren() << "----> number of children of u->b->b" << std::endl;

	std::cout << truthp_itr->parent(0)->parent(0)->absPdgId() << "--------->b-quark from b-quark -> parent pdgId" << std::endl;

	//bPTFromTquark->Fill( truthp_itr->pt() * 0.001 );
	}
      */

      if (truthp_itr->parent(0)->absPdgId() == 6) 
	bPTFromTquark->Fill( truthp_itr->pt() * 0.001 );

      if (truthp_itr->parent(0)->absPdgId() == 21)
	bPTFromGluon->Fill( truthp_itr->pt() * 0.001 );
      
      if (truthp_itr->parent(0)->absPdgId() == 2) {

	dquarknParent->Fill( truthp_itr->parent(0)->nParents() );
	bPTFromUquark->Fill( truthp_itr->pt() * 0.001 );



	//output the parent of all b-quarks
	/**********
	std::cout << truthp_itr->nParents() << "--------->b-quark n parent" << std::endl;
	std::cout << truthp_itr->parent(0)->pdgId() << "--------->b-quark parent 0 pdgId" << std::endl;
	std::cout << truthp_itr->parent(0)->nParents() << "--------->b-quark parent_0's parents number " << std::endl;
	std::cout << truthp_itr->parent(0)->parent(0)->pdgId() << "--------->b-quark parent_0's parent 0's pdgId " << std::endl;
	

	
	std::cout << truthp_itr->parent(1)->pdgId() << "--------->b-quark parent 1 pdgId" << std::endl;
	std::cout << truthp_itr->parent(1)->nParents() << "--------->b-quark parent_1's parents number " << std::endl;
	std::cout << truthp_itr->parent(1)->parent(0)->pdgId() << "--------->b-quark parent_1's parent 0's pdgId " << std::endl;
	


	
	std::cout << "***************************************" << std::endl;
	std::cout << truthp_itr->parent(0)->parent(0)->pdgId() << "--------->u parent pdgID" << std::endl;
	std::cout << truthp_itr->parent(0)->parent(0)->nChildren() << "--------->u parent n children" << std::endl;
	std::cout << truthp_itr->parent(0)->nChildren() << "--------->u quark n children" << std::endl;

	**********/ 


	//	dpPdgId.push_back( truthp_itr->parent(0)->parent(0)->absPdgId() );
	//	dquarkParentPdgId->Fill( truthp_itr->parent(0)->parent(0)->absPdgId() );


      }

    }
  } // end of for
  
  tQuark.push_back( bQuark.at(0)->parent(0) );
  tQuark.push_back( bQuark.at(1)->parent(0) );

  // b1,2,3,4 pT tree
  b1PT = bQuark.at(0)->pt() * 0.001;
  b2PT = bQuark.at(1)->pt() * 0.001;

  // b1,2,3,4 Eta tree
  b1Eta = bQuark.at(0)->eta();
  b2Eta = bQuark.at(1)->eta();

  // b1,2,3,4 Phi tree
  b1Phi = bQuark.at(0)->phi();
  b2Phi = bQuark.at(1)->phi();

  // b1,2,3,4 m tree
  b1M = bQuark.at(0)->m();
  b2M = bQuark.at(1)->m();

  b1Px = bQuark.at(0)->px();
  b1Py = bQuark.at(0)->py();
  b1Pz = bQuark.at(0)->pz();

  b2Px = bQuark.at(1)->px();
  b2Py = bQuark.at(1)->py();
  b2Pz = bQuark.at(1)->pz();

  // b1b2DeltaPhi 
  temp = b1Phi - b2Phi;
  if ( temp < -TMath::Pi() )
    temp = 2*TMath::Pi() + temp;
  else if ( temp > TMath::Pi() )
    temp = 2*TMath::Pi() - temp;
  else
    temp = std::abs( temp );
  b1b2DeltaPhi = temp;



  // analysis code for JET level
  /****************************************************************************************************************************************************************/
  //NEVER touch the code before this line EVER!


  // get the jet containter of interest
  const xAOD::JetContainer *jets = NULL;
  ANA_CHECK(event->retrieve(jets, "AntiKt4EMTopoJets"));
  nOfJets = jets->size();  


  std::pair< xAOD::JetContainer*, xAOD::ShallowAuxContainer* > jets_copy = xAOD::shallowCopyContainer( *jets );
  xAOD::JetContainer* jets_calib = jets_copy.first;

  for (auto jet : *jets_calib) {
    // Correct the jet
    m_jetCalibration->applyCorrection(*jet);

    // Mark as selected for the purposes of OLR later.
    jet->auxdecor<char>("selected") = '1';
  }

 
  //Info("execute()", "  number of jets = %lu", jets->size());

  /**********code verion 1 */

  std::vector<const xAOD::Jet*> btaggedjets;
  btaggedjets.clear();

  //  bJetLeadingPtTest.clear();

  // loop over jet container and select b-jets --> loop code same with Matthew's for comparison
  for (const auto &jet_itr : *jets_calib) {
    if (jet_itr->pt() * 0.001 > 20) {
      if (TMath::Abs(jet_itr->eta()) > 2.5) continue;
      if (!(btagtool->accept(*jet_itr))) continue;
      if ((jet_itr->pt() * 0.001 < 60) && (TMath::Abs(jet_itr->eta()) < 2.4))
        if (hjvtagup->updateJvt(*jet_itr) < 0.59) continue;
      
      btaggedjets.push_back(jet_itr);
    }
  }
  btaggedjetsNumber = btaggedjets.size(); //btaggedjetsNumber is a variable that I l plot, but is not necessary here

  // value initialization
  bJetLeadingPt = -9.0;
  bJetLeadingMass = -9.0;
  bJetSubleadingPt = -9.0;
  bJetSubleadingMass  = -9.0;
  bJetThirdleadingPt = -9.0;
  bJetThirdleadingMass = -9.0;
  bJetFourthleadingPt = -9.0;
  bJetFourthleadingMass = -9.0;
  bJetFifthleadingPt = -9.0;
  bJetFifthleadingMass = -9.0;

  j1j2Mass = -9.0;
  j1j3Mass = -9.0;
  j1j2DeltaR = -9.0;
  j1j2MassDeltaRSmall = -9.0;
  j1j2MassDeltaRLarge = -9.0;

  nOfJetsCounter = 0;
  nOfJetsOpposite = 0;

  bjetsInCone.clear();

  j1j2MassCone = -9.0;

  // sort the vector of b-tagged jets by their pT from greatest to least
  if (btaggedjetsNumber != 0) {
    std::sort(btaggedjets.begin(), btaggedjets.end(), jetpTCompare);
  
    bJetLeadingPt = btaggedjets.at(0)->pt() * 0.001;
    bJetLeadingMass = btaggedjets.at(0)->m() * 0.001;


    if (btaggedjetsNumber >= 2) {
      bJetSubleadingPt = btaggedjets.at(1)->pt() * 0.001;
      bJetSubleadingMass = btaggedjets.at(1)->m() * 0.001;
      j1j2Mass = ( btaggedjets.at(0)->p4() + btaggedjets.at(1)->p4() ).M() * 0.001;
      
      //
      if (j1j2Mass > 60 && j1j2Mass < 100) {
	// get the direction of the higgs
	// flip the direction
	reconstructHiggs = -(btaggedjets.at(0)->p4() + btaggedjets.at(1)->p4());
      }
      
      // count jets in the cone
      std::vector<const xAOD::Jet_v1*>::iterator jetItr = btaggedjets.begin();
      std::vector<const xAOD::Jet_v1*>::iterator jetEnd = btaggedjets.end();

      jetItr = jetItr + 2;

      for ( ;jetItr != jetEnd ; jetItr++) {
	if ( reconstructHiggs.DeltaR( (*jetItr)->p4() ) < 3 ) {
	  nOfJetsCounter++;
	  bjetsInCone.push_back( *jetItr );
	}


      }

      nOfJetsOpposite = nOfJetsCounter;
      

      // plot mass of bbbar
      if (nOfJetsOpposite >= 2) {
	std::sort(bjetsInCone.begin(), bjetsInCone.end(), jetpTCompare);
	
	j1j2MassCone = ( bjetsInCone.at(0)->p4() + bjetsInCone.at(1)->p4() ).M() * 0.001;
      }

      // normalize to cross section









      // about DeltaR
      j1j2DeltaR = (btaggedjets.at(0)->p4()).DeltaR( btaggedjets.at(1)->p4() );

      if ((btaggedjets.at(0)->p4()).DeltaR( btaggedjets.at(1)->p4() ) < 1.9)
	j1j2MassDeltaRSmall = ( btaggedjets.at(0)->p4() + btaggedjets.at(1)->p4() ).M() * 0.001;
      else 
	j1j2MassDeltaRLarge = ( btaggedjets.at(0)->p4() + btaggedjets.at(1)->p4() ).M() * 0.001;
    
	}








    if (btaggedjetsNumber >= 3) {
      bJetThirdleadingPt = btaggedjets.at(2)->pt() * 0.001;
      bJetThirdleadingMass = btaggedjets.at(2)->m() * 0.001;
      j1j3Mass = ( btaggedjets.at(0)->p4() + btaggedjets.at(2)->p4() ).M() * 0.001;

    }

    if (btaggedjetsNumber >= 4) {
      bJetFourthleadingPt = btaggedjets.at(3)->pt() * 0.001;
      bJetFourthleadingMass = btaggedjets.at(3)->m() * 0.001;
    }

    if (btaggedjetsNumber >= 5) {
      bJetFifthleadingPt = btaggedjets.at(4)->pt() * 0.001;
      bJetFifthleadingMass = btaggedjets.at(4)->m() * 0.001;
    }

  }
   /*end of code version 1**********/

  /********** code version 0
  // loop over the jets in the container
  xAOD::JetContainer::const_iterator jet_itr = jets->begin();
  xAOD::JetContainer::const_iterator jet_end = jets->end();
   
  //  std::vector<const xAOD::TruthParticle*> tQuark;
  
  std::vector<const xAOD::Jet *> jetsTagged;
  
  //----> loop over code from Peilong
  for( ; jet_itr != jet_end; ++jet_itr ) {
    bJetPt->Fill( (*jet_itr)->pt() * 0.001);
    // call BTagging selection tool: pT > 20 GeV, Eta < 2.5
    if (btagtool->accept( *jet_itr )) {
    
      if (hjvtagup->updateJvt(**jet_itr) < 0.59 && ((*jet_itr)->pt() * 0.001) < 60 && TMath::Abs( (*jet_itr)->eta() ) < 2.4) 	{
	
      }
      else {

	bJetTaggedUpdatedPt->Fill( hjvtagup->updateJvt(**jet_itr) * (*jet_itr)->pt() * 0.001);
	//std::cout << "JVT -> " << hjvtagup->updateJvt(**jet_itr) * (*jet_itr)->pt() * 0.001 << std::endl;
	jetsTagged.push_back(*jet_itr);


      }     

    }
    //Info("execute()", "  jet pt = %.2f GeV", ((*jet_itr)->pt() * 0.001)); // just to print out something

  } // end for loop over jets
  
  nOfJetsTagged = jetsTagged.size();
  for (const auto &jetItr : jetsTagged) {
    bJetTaggedPt->Fill(jetItr->pt() * 0.001);
  }

  end of code version 0 **********/

  

  // Fill trees
  tree->Fill();


  // Clean up the shallow copy of the jets container
  if (jets_copy.first != nullptr)
    delete jets_copy.first;
  if (jets_copy.second != nullptr)
    delete jets_copy.second;
  

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODttbarBackground :: postExecute ()
{
  // Here you do everything that needs to be done after the main event
  // processing.  This is typically very rare, particularly in user
  // code.  It is mainly used in implementing the NTupleSvc.
  return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODttbarBackground :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.  This is different from histFinalize() in that it only
  // gets called on worker nodes that processed input events.

  ANA_CHECK_SET_TYPE (EL::StatusCode); // set type of return code you are expecting (add to top of each function once)

  xAOD::TEvent* event = wk()->xaodEvent();

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode xAODttbarBackground :: histFinalize ()
{
  // This method is the mirror image of histInitialize(), meaning it
  // gets called after the last event has been processed on the worker
  // node and allows you to finish up any objects you created in
  // histInitialize() before they are written to disk.  This is
  // actually fairly rare, since this happens separately for each
  // worker node.  Most of the time you want to do your
  // post-processing on the submission node after all your histogram
  // outputs have been merged.  This is different from finalize() in
  // that it gets called on all worker nodes regardless of whether
  // they processed input events.
  return EL::StatusCode::SUCCESS;
}
