#ifndef MyBackgroundAnalysis_MyxAODBackgroundAnalysis_H
#define MyBackgroundAnalysis_MyxAODBackgroundAnalysis_H

#include <EventLoop/Algorithm.h>
#include <TTree.h>
#include <TH1.h>
#include "xAODBTaggingEfficiency/BTaggingSelectionTool.h"
#include "JetMomentTools/JetVertexTaggerTool.h"
#include <vector>
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "TLorentzVector.h"
#include "xAODJet/JetContainer.h"
#include "JetCalibTools/JetCalibrationTool.h"
#include <string>

#include "HHRecoUtil/NaiveConeMatch.h"


class MyxAODBackgroundAnalysis : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  //  Tree *myTree; //!
  // TH1 *myHist; //!
  TTree *tree;//!


  // b-jet level histogram or tree
  /***********************************************************************************/
  // BTaggingSelectionTool initialization

  std::string outputName;

  BTaggingSelectionTool *btagtool = NULL; //!

  // JetVertexTaggerTool initialization
  JetVertexTaggerTool* pjvtag = 0; //!
  ToolHandle<IJetUpdateJvt> hjvtagup; //!
  bool fail = false;//!

  int nOfJets;//!
  int nOfJetsTagged; //!

  std::vector<double> bJetPt;//!
  std::vector<double> bJetTaggedPt;//!
  std::vector<double> bJetTaggedUpdatedPt;//!

  double bJetLeadingPt;//!
  double bJetSubleadingPt;//!
  double bJetThirdleadingPt;//!
  double bJetFourthleadingPt;//!
  double bJetFifthleadingPt;//!

  double bJetLeadingMass;//!
  double bJetSubleadingMass;//!
  double bJetThirdleadingMass;//!
  double bJetFourthleadingMass;//!
  double bJetFifthleadingMass;//!

  double bJetMass1plus2;//!

  int btaggedjetsNumber; //! 
  
  // TLorentzVector *j1j2;//!
  double j1j2Mass;//!
  double j1j2DeltaR;//!

  double j1j2MassDeltaRSmall;//!
  double j1j2MassDeltaRLarge;//!

  double j1j3Mass;//!

  TLorentzVector reconstructHiggs;//! 

  int nOfJetsCounter;//!
  int nOfJetsOpposite;//!

  double j1j2MassCone;//!

  std::vector<const xAOD::Jet*> bjetsInCone;//!

  JetCalibrationTool* m_jetCalibration; //!

  // member function 
  /*******************************************/

  // this is a standard constructor
  MyxAODBackgroundAnalysis ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(MyxAODBackgroundAnalysis, 1);
};

#endif
